#!/bin/bash

## install composer
## https://getcomposer.org/download/
wget https://getcomposer.org/composer.phar

## get dependencies
php composer.phar install --no-dev

## run codesmell:

return=0

echo "------------------------"
echo "Checking code-standard:"
php vendor/bin/phpcs --ignore=*/vendor/*,*/tests/* --standard=phpcs-ruleset.xml -n ../src/
if [ $? -ne 0 ] ; then
	echo "Code-standard failed!"
	return=$(($return + 1))
fi

echo "------------------------"
echo "Running messdetection:"
php vendor/bin/phpmd --exclude "*/vendor/*,*/tests/*" ../src/ text naming,codesize,design,unusedcode
if [ $? -ne 0 ] ; then
	echo "Mess detection failed!"
	return=$(($return + 2))
fi

echo "------------------------"
echo "Check for duplicated code"
php vendor/bin/phpcpd ../src/
if [ $? -ne 0 ] ; then
	echo "Too much duplicated code!"
	return=$(($return + 4))
fi

exit $return

